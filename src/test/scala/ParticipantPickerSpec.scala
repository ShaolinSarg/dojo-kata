package newcastle.dojo.kata

import scala.concurrent.duration._
import scala.util.Random
import org.scalatest.{ FlatSpec, Matchers }

class ParticipantPickerSpec extends FlatSpec with Matchers {

  case class Answer(timeTaken: FiniteDuration)
  class Participant(name: String, answersGiven: List[Answer]) {
    override def toString = name
    def questionsAnswered = answersGiven.length
    def averageTimeOnAnswers = {
      if (questionsAnswered == 0) 0 seconds
      else {
        val totalTime = answersGiven.map(_.timeTaken).foldLeft(0 seconds)(_ + _)
        totalTime / questionsAnswered
      }
    }
  }

  val andrew = new Participant("Andrew", List(new Answer(200 minutes)))
  val bernice = new Participant("Bernice", List(new Answer(100 minutes)))
  val calvin = new Participant("Calvin", List(new Answer(30 minutes)))
  val delia = new Participant("Delia", List(new Answer(15 minutes), new Answer(45 minutes)))
  val edwin = new Participant("Edwin", List(new Answer(600 minutes), new Answer(300 minutes)))
  val frances = new Participant("Frances", List(new Answer(30 minutes)))
  val tosney = new Participant("Tosney", List(new Answer(30 minutes)))

  val candidates = Set(andrew, bernice, calvin, delia, edwin, frances, tosney)
  val holidayers = Set(tosney)

  // 1) The participant who just answered a question cannot be allocated to answer his own question
  "Next question legal candidates" should "not contain the participant who just answered a question" in {
    nextQuestionLegalCandidates(candidates, holidayers, andrew, bernice) should not contain andrew
  }

  // 2) The participant who just gave them their question cannot be allocated to answer the next question
  it should "not contain the previous asker" in {
    nextQuestionLegalCandidates(candidates, holidayers, andrew, bernice) should not contain bernice
  }

  // 3) Any participant who is on holiday (Currently a participant called Tosney) cannot be allocated to answer the next question
  it should "not contain participants on holiday" in {
    nextQuestionLegalCandidates(candidates, holidayers, andrew, bernice) should not contain tosney
  }

  // 4) The participant who has answered the least number of questions out of those remaining should be chosen
  "Questions answered" should "be one if exactly one answered" in {
    andrew.questionsAnswered shouldBe 1
  }
  "Fewest answers" should "contain only those who have answered the fewest questions" in {
    fewestAnswers(candidates) shouldBe Set(andrew, bernice, calvin, frances, tosney)
  }

  // 5) In the event of a tie in 4) above, the participant who has spent the LEAST
  // average time on answering questions should be chosen
  "Shortest average time on answers" should "be the set of participants with the shortest average time" in {
    shortestAverageTimeOnAnswers(candidates) shouldBe Set(calvin, delia, frances, tosney)
  }

  // 6) If there's still a tie after 5) pick one of the participants using some other criteria 
  // (random or alphabetical order or something else (such as ability to spell their first name correctly) - you choose)
  "Correct spelling" should "be Graeme" in {
    correctSpelling shouldBe "Graeme"
  }
  "Next pick" should "come from the set of candidates meeting all of the above criteria" in {
    Set(calvin, frances) should contain(nextPick(candidates, holidayers, andrew, bernice).get)
  }

  def nextQuestionLegalCandidates(candidates: Set[Participant],
    holidayers: Set[Participant],
    previousAnswerer: Participant,
    previousAsker: Participant) = {
    val peopleWeCantPick = holidayers + previousAnswerer + previousAsker
    candidates filter (p => !(peopleWeCantPick contains p))
  }

  // minSet is an intermediate function to return the set of all Ts sharing the minimum of some value.
  // I expect there's something in the scala library to do this for me but I can't easily find it.
  // 'compare' should be positive if the first is greater than the second, negative if the reverse, else 0.
  def minSet[T](participants: Set[T])(compare: (T, T) => Int) = {
    def accMinSet(acc: Set[T], remaining: Set[T]): Set[T] = {
      if (remaining.isEmpty) acc
      else if (acc.isEmpty) accMinSet(Set(remaining.head), remaining.tail)
      else if (compare(acc.head, remaining.head) > 0) accMinSet(Set(remaining.head), remaining.tail)
      else if (compare(acc.head, remaining.head) < 0) accMinSet(acc, remaining.tail)
      else accMinSet(acc + remaining.head, remaining.tail)
    }
    accMinSet(Set(), participants)
  }

  def fewestAnswers(candidates: Set[Participant]) = {
    minSet(candidates) {
      (x, y) => x.questionsAnswered - y.questionsAnswered
    }
  }

  def shortestAverageTimeOnAnswers(candidates: Set[Participant]) = {
    minSet(candidates) {
      // If you're looking for a new Scala Bomb question, you can make this pretty:
      (x, y) => (x.averageTimeOnAnswers - y.averageTimeOnAnswers).toSeconds.toInt
    }
  }

  final def correctSpelling = "Graeme"

  def getRandomElementFrom[T](set: Set[T]): Option[T] = {
    if (set.isEmpty) None
    else {
      val rnd = new Random
      Some(set.toVector(rnd.nextInt(set.size)))
    }
  }

  def nextPick(candidates: Set[Participant],
    holidayers: Set[Participant],
    previousAnswerer: Participant,
    previousAsker: Participant) = {
    getRandomElementFrom(
      shortestAverageTimeOnAnswers(
        fewestAnswers(
          nextQuestionLegalCandidates(candidates, holidayers, previousAnswerer, previousAsker))))
  }
}