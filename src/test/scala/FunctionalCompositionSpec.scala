package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }

class FunctionalComposition extends FlatSpec with Matchers {

  def takeAwayThree(total: Int): Int = total - 3
  def divideByTwo(total: Int): Int = total / 2
  def addSix(total: Int): Int = total + 6

  def composeFunction(): (Int) => Int = addSix _ andThen divideByTwo _ andThen takeAwayThree _
  def composeFunction2(): (Int) => Int = addSix _ andThen takeAwayThree _ andThen divideByTwo _

  val myCalculation = composeFunction()
  val myCalculation2 = composeFunction2()

  "Take away three" should "subtract three from input value" in {
    takeAwayThree(2) shouldBe -1
  }

  "Divide by two" should "divide input value by 2" in {
    divideByTwo(5) shouldBe 2
  }

  "Add six" should "add six to input value" in {
    addSix(-6) shouldBe 0
  }

  "My first calculation" should "add 6, divide by 2 and subtract 3" in {
    myCalculation(2) shouldBe 1
  }

  "My second calculation" should "add 6, subtract 3 and divide by 2" in {
    myCalculation2(3) shouldBe 3
  }
}