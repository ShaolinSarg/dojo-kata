package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }
import org.scalatest.prop.PropertyChecks
import org.scalacheck.Gen
import java.time.LocalTime

object WordClock {
  val Prefix = "IT IS"
  val Quarter = "QUARTER"
  val Half = "HALF"
  val Past = "PAST"
  val To = "TO"
  val Five = "FIVE"
  val Ten = "TEN"
  val Twenty = "TWENTY"
  val OClock = "O'CLOCK"
  val Hours = Map(1 -> "ONE", 2 -> "TWO", 3 -> "THREE", 4 -> "FOUR", 5 -> "FIVE", 6 -> "SIX", 7 -> "SEVEN", 8 -> "EIGHT", 9 -> "NINE", 10 -> "TEN", 11 -> "ELEVEN", 12 -> "TWELVE")

  def main(args: Array[String]): Unit = {
    println(new WordClock(LocalTime.of(2, 23)).show)
  }
}

class WordClock(time: LocalTime) {

  import WordClock._

  def beforeHalfPast(minutes: Int) = minutes < 31

  def show = {

    val hours =
      if (time.getMinute() > 30)
        Hours(time.getHour() + 1)
      else Hours(time.getHour())

    val minutes = time.getMinute() match {
      case x if x > 0 && x <= 5 => Five
      case x if x > 5 && x <= 10 => Ten
      case x if x > 10 && x <= 15 => Quarter
      case x if x > 15 && x <= 20 => Twenty
      case x if x > 20 && x <= 25 => s"$Twenty $Five"
      case x if x > 25 && x <= 30 => Half
      case x if x > 30 && x <= 35 => s"$Twenty $Five"
      case x if x > 35 && x <= 40 => Twenty
      case x if x > 40 && x <= 45 => Quarter
      case x if x > 45 && x <= 50 => Ten
      case x if x > 50 && x <= 55 => Five
      case _ => ""
    }

    val toOrPast = time.getMinute() match {
      case x if x > 55 || x == 0 => "";
      case x if beforeHalfPast(x) => Past
      case _ => To
    }

    val onTheHour = time.getMinute() match {
      case x if x > 55 || x == 0 => OClock
      case _ => ""
    }

    s"$Prefix $minutes $toOrPast $hours $onTheHour"
  }
}

class WordClockSpec extends FlatSpec with Matchers with PropertyChecks {

  "wordClock" should "always include 'IT IS'" in {
    val clock = new WordClock(LocalTime.of(2, 20))

    clock.show should startWith("IT IS")
  }

  it should "always return HALF at half past the hour" in {
    val clock = new WordClock(LocalTime.of(2, 30))

    clock.show should include("HALF")
    clock.show should include("PAST")
  }

  it should "always return QUARTER and PAST at quarter past the hour" in {
    val clock = new WordClock(LocalTime.of(2, 15))

    clock.show should include("QUARTER")
    clock.show should include("PAST")
  }

  it should "always return QUARTER and TO at quarter to the hour" in {
    val clock = new WordClock(LocalTime.of(2, 45))

    clock.show should include("QUARTER")
    clock.show should include("TO")
  }

  it should "always return O'CLOCK on the hour" in {
    val clock = new WordClock(LocalTime.of(2, 0))

    clock.show should include("O'CLOCK")

  }

  val genTo = Gen.chooseNum(31, 55)
  val genPast = Gen.chooseNum(1, 30)

  it should "always return PAST before 31 minutes past" in {
    forAll(genPast) { (mins: Int) =>
      val clock = new WordClock(LocalTime.of(2, mins))

      clock.show should include("PAST")
    }
  }

  it should "always return TO after 30 minutes past" in {
    forAll(genTo) { (mins: Int) =>
      val clock = new WordClock(LocalTime.of(2, mins))

      clock.show should include("TO")
    }
  }

  it should "return FIVE when the time is 3 minutes past the hour" in {
    val clock = new WordClock(LocalTime.of(2, 3))

    clock.show should include("FIVE")
  }

  it should "return TEN when the time is 6 minutes past the hour" in {
    val clock = new WordClock(LocalTime.of(2, 6))

    clock.show should include("TEN")
  }

  it should "return TWENTY PAST when the time is 19 minutes past the hour" in {
    val clock = new WordClock(LocalTime.of(2, 19))

    clock.show should include("TWENTY")
  }

  it should "return TWENTY FIVE PAST- when the time is 24 minutes past the hour" in {
    val clock = new WordClock(LocalTime.of(2, 24))

    clock.show should include("TWENTY")
    clock.show should include("FIVE")
    clock.show should include("PAST")
  }

  it should "return TWENTY FIVE TO when the time is 32 minutes past the hour" in {
    val clock = new WordClock(LocalTime.of(2, 32))

    clock.show should include("TWENTY")
    clock.show should include("FIVE")
    clock.show should include("TO")
  }

  it should "return TO THREE when the time is after half past 2 but before 3" in {
    val clock = new WordClock(LocalTime.of(2, 32))

    clock.show should include("TWENTY")
    clock.show should include("FIVE")
    clock.show should include("TO")
    clock.show should include("THREE")
  }

  val genHour = Gen.chooseNum(1, 12)

  it should "return the OCLOCK for each hour" in {
    forAll(genHour) { (hour: Int) =>
      val clock = new WordClock(LocalTime.of(hour, 0))

      clock.show should include("O'CLOCK")
    }
  }

  it should "return the word of the hour, on the hour" in {
    val clock4 = new WordClock(LocalTime.of(4, 0))
    clock4.show should include("FOUR")
    val clock5 = new WordClock(LocalTime.of(5, 0))
    clock5.show should include("FIVE")

  }

  it should "round up 1:59 to TWO OCLOCK" in {
    val clock = new WordClock(LocalTime.of(1, 59))
    clock.show should include("TWO")
    clock.show should include("O'CLOCK")
  }

  it should "not explode just because it is a 12 hour clock" in {
    val clock = new WordClock(LocalTime.of(13, 1))
    clock.show should include("FIVE")
    clock.show should include("PAST")
    clock.show should include("ONE")
  }
}

// IT IS HALF  TEN
// QUARTER  TWENTY
// FIVE MINUTES TO
// PAST  ONE THREE
// TWO  FOUR  FIVE
// SIX SEVEN EIGHT
// NINE TEN ELEVEN
// TWELVE  O'CLOCK

// IT IS       TEN

//      MINUTES   
// PAST  

// TWELVE