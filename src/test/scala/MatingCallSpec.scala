package newcastle.dojo.kata

import org.scalatest.{ FunSpec, Matchers }

trait Animal { def matingCall: String }
case class Rhino() extends Animal { def matingCall: String = "HUURGHHH" }
case class RoadRunner() extends Animal { def matingCall: String = "meep meep" }

class MatingCallSpec() extends FunSpec with Matchers {

  def rhino = new Rhino
  def roadRunner = new RoadRunner

  def animalAndMatingCall(animal: Animal): String = "A " + animal.getClass.getSimpleName + " goes " + animal.matingCall

  describe("matingCall") {
    describe("for a rhino") {
      it("should be HUURGHHH") {
        rhino.matingCall shouldBe "HUURGHHH"
      }
    }
  }

  describe("for a RoadRunner") {
    it("should be meep meep") {
      roadRunner.matingCall shouldBe "meep meep"
    }
  }

  describe("animalAndMatingCall") {
    describe("given a horny Rhino") {
      it("should return \"A Rhino goes HUURGHHH\"") {
        animalAndMatingCall(rhino) shouldBe "A Rhino goes HUURGHHH"
      }
    }
    describe("given a RoadRunner") {
      it("should return \"A RoadRunner goes meep meep\"") {
        animalAndMatingCall(roadRunner) shouldBe "A RoadRunner goes meep meep"
      }
    }

  }

}