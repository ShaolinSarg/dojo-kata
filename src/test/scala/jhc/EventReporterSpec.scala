package jhc

import org.scalatest._

class EventReporterSpec extends FlatSpec with Matchers {

  //You have a test runner that sends you events corresponding to the outcome of tests and events that happen within those tests
  //but we want to transform them into a different format for Zucchini

  //Scalatest Input model - External library - change nothing in here /////////
  ///////////////////////////////////////////////////////////////
  sealed trait Event {
    val timestamp: Long
    val name: String
  }

  case class TestSucceeded(
    timestamp: Long,
    name: String,
    testText: String,
    recordedEvents: Seq[RecordedEvent]) extends Event

  case class TestFailed(
    timestamp: Long,
    name: String,
    testText: String,
    recordedEvents: Seq[RecordedEvent]) extends Event

  //a recorded event is an event with a message corresponding to a step being executed like "Given xxx"
  case class RecordedEvent(timestamp: Long, name: String) extends Event

  /////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////////////////////////////
  //Output model - Zucchini -  change nothing in here:
  //Should Output a TestReport:
  //in our desired output each Test report has a name describing the test, some text and a  list of the steps inside it:
  case class TestReport(val testName: String, val text: String, val steps: Seq[Step])

  //each step (such as "Given i have 2 points") has a name and a result:
  case class Step(val stepName: String, val result: StepResult)

  //a test result should consist of the duration of that step in ms and whether the step passed or failed:
  case class StepResult(val duration: Long, val status: StepStatus)

  //(you can assume an event ends when the next one begins)

  //a step status can be passed or failed:
  sealed trait StepStatus

  object Passed extends StepStatus

  object Failed extends StepStatus

  ///////////////////////////////////////////////////////////////////////////////////////////////

  //Question:
  /*
  Please implement a reporter that can handle events sent by the test runner and produce test reports
  Try to minimise duplication as much as possible and avoid vars & mutable state.
  Can you implement one method that handles both cases?
   */
  class Reporter {
    def apply(event: Event): TestReport = {
      event match {
        case ev: TestSucceeded => respToReport(ev)
        case ev: TestFailed => respToReport(ev)
        case _ => throw new IllegalArgumentException("Ooops, needs to have events inside it to work out durations")
      }
    }
  }

  val reporter = new Reporter

  def convertEventToStep(event: RecordedEvent, duration: Long, status: StepStatus): Step = {
    Step(event.name, StepResult(duration, status))
  }

  def timeEvents(event: Event): Seq[Long] = {
    def iter(end: Long, rem: Seq[RecordedEvent], times: List[Long]): List[Long] = {
      if (rem.isEmpty) times
      else iter(rem.head.timestamp, rem.tail, (end - rem.head.timestamp) :: times)
    }

    event match {
      case r: RecordedEvent => Nil
      case ev: TestSucceeded => iter(ev.timestamp, ev.recordedEvents.reverse, Nil)
      case ev: TestFailed => iter(ev.timestamp, ev.recordedEvents.reverse, Nil)
    }
  }

  "timeEvents" should "time events" in {
    val ev = TestSucceeded(5000, "answering a question", "all fine",
      Seq(
        RecordedEvent(4050, "Given I have 2 points"),
        RecordedEvent(4080, "When I answer a question correctly"),
        RecordedEvent(4100, "Then i have 3 points")
      ))

    timeEvents(ev) shouldBe Seq(30, 20, 900)
  }

  def respToReport(event: Event): TestReport = {
    event match {
      case ev: TestSucceeded => {
        val eventsAndTimes = ev.recordedEvents.zip(timeEvents(ev))
        TestReport(ev.name, ev.testText, eventsAndTimes.map(item => convertEventToStep(item._1, item._2, Passed)))
      }
      case ev: TestFailed => {
        val eventsAndTimes = ev.recordedEvents.zip(timeEvents(ev))
        val report = TestReport(ev.name, ev.testText, eventsAndTimes.map(item => convertEventToStep(item._1, item._2, Passed)))
        report.copy(steps = report.steps.reverse.tail.reverse :+ report.steps.reverse.head.copy(result = report.steps.reverse.head.result.copy(status = Failed)))
      }
      case _ => throw new IllegalArgumentException("Ooops, not a success or fail")
    }

  }

  "respToReport" should "given a success return the report" in {
    val ev = TestSucceeded(5000, "answering a question", "all fine",
      Seq(
        RecordedEvent(4050, "Given I have 2 points"),
        RecordedEvent(4080, "When I answer a question correctly"),
        RecordedEvent(4100, "Then i have 3 points")
      ))

    respToReport(ev) shouldBe TestReport("answering a question", "all fine",
      Seq(
        Step("Given I have 2 points", StepResult(30, Passed)),
        Step("When I answer a question correctly", StepResult(20, Passed)),
        Step("Then i have 3 points", StepResult(900, Passed))
      ))
  }

  "reporter" should "report the durations and status of steps in a successful test" in {
    assert(
      reporter(
        TestSucceeded(5000, "answering a question", "all fine",
          Seq(
            RecordedEvent(4050, "Given I have 2 points"),
            RecordedEvent(4080, "When I answer a question correctly"),
            RecordedEvent(4100, "Then i have 3 points")
          ))
      )
        == TestReport("answering a question", "all fine",
          Seq(
            Step("Given I have 2 points", StepResult(30, Passed)),
            Step("When I answer a question correctly", StepResult(20, Passed)),
            Step("Then i have 3 points", StepResult(900, Passed))
          )))
  }

  it should "mark last step as failed in a failed test" in {
    assert(
      reporter(
        TestFailed(5000, "answering a question", "all fine",
          Seq(
            RecordedEvent(4050, "Given I am the current holder and I have 2 points"),
            RecordedEvent(4060, "And I've answered a question"),
            RecordedEvent(4080, "When the bomb goes off"),
            RecordedEvent(4100, "Then i have 1 point :(")
          ))
      )
        == TestReport("answering a question", "all fine",
          Seq(
            Step("Given I am the current holder and I have 2 points", StepResult(10, Passed)),
            Step("And I've answered a question", StepResult(20, Passed)),
            Step("When the bomb goes off", StepResult(20, Passed)),
            Step("Then i have 1 point :(", StepResult(900, Failed))
          )))
  }
}