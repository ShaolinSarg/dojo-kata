package newcastle.dojo.kata

import org.scalatest.{ FunSpec, Matchers }

object KleeneTruthTableGenerator extends App {
  object KleeneValue extends Enumeration {
    type KleeneValue = Value
    val T, F, U = Value
  }

  import KleeneValue._

  def and(x: KleeneValue, y: KleeneValue): KleeneValue = (x, y) match {
    case (T, T) => T
    case (T, F) => F
    case (T, U) => U
    case (F, T) => F
    case (F, F) => F
    case (F, U) => U
    case (U, T) => U
    case (U, F) => F
    case (U, U) => U
  }

  def or(x: KleeneValue, y: KleeneValue): KleeneValue = (x, y) match {
    case (T, T) => T
    case (T, F) => T
    case (T, U) => T
    case (F, T) => T
    case (F, F) => F
    case (F, U) => U
    case (U, T) => T
    case (U, F) => U
    case (U, U) => U
  }

  def printTruthTable(f: (KleeneValue, KleeneValue) => KleeneValue): Unit = {
    println("x\ty\tResult")
    truthTable(f).foreach(row => println(row.mkString("\t")))
  }

  def truthTable(doTheMagic: (KleeneValue, KleeneValue) => KleeneValue): Seq[String] = {
    for (x <- Seq(T, F, U); y <- Seq(T, F, U)) yield s"$x$y${doTheMagic(x, y)}"
  }
}

class KleeneTruthTableGeneratorSpec extends FunSpec with Matchers {

  import KleeneTruthTableGenerator._
  import KleeneTruthTableGenerator.KleeneValue._

  describe("and") {
    describe("given T and F") {
      it("should return F") {
        and(T, F) shouldBe F
      }
    }
    describe("given T and U") {
      it("should return F") {
        and(T, U) shouldBe U
      }
    }
    describe("given U and F") {
      it("should return F") {
        and(U, F) shouldBe F
      }
    }
  }

  describe("or") {
    describe("given T and F") {
      it("should return T") {
        or(T, F) shouldBe T
      }
    }
    describe("given T and U") {
      it("should return T") {
        or(T, U) shouldBe T
      }
    }
    describe("given U and F") {
      it("should return U") {
        or(U, F) shouldBe U
      }
    }
  }

  describe("and(or(x, y), y)") {
    describe("given T and F") {
      it("should return F") {
        and(or(T, F), F) shouldBe F
      }
    }
    describe("given T and U") {
      it("should return T") {
        and(or(T, U), U) shouldBe U
      }
    }
    describe("given U and F") {
      it("should return U") {
        and(or(U, F), F) shouldBe F
      }
    }
  }

  describe("truthTable") {
    describe("given a function and(or(x, y), y)") {
      it("should print the right values") {
        val func = (x: KleeneValue, y: KleeneValue) => and(or(x, y), y)

        truthTable(func).head shouldBe "TTT"
        truthTable(func).tail.head shouldBe "TFF"
        truthTable(func).tail.tail.head shouldBe "TUU"
      }
    }
  }
}