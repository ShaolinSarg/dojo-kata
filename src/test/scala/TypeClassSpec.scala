/*
 * Scala has a powerful feature called typeclasses - there is an excellent explanation of them here: 
 *   http://danielwestheide.com/blog/2013/02/06/the-neophytes-guide-to-scala-part-12-type-classes.html
 *
 * In application-services typeclasses are used in many places, including most obviously:
 *   - Converting domain classes to JSON
 *   - Reading and writing mapped database columns
 *   - EntitySecurity
 *
 * Here you task is to implement instances of a typeclass called SillyPrint.
 * This is a small example that shows how a `silly` method can be added to types
 * we don't have any control over including those in the standard Scala library
 */

import org.scalatest.FlatSpec
import org.scalatest.Matchers

// Typeclass interface witnessing that a type A can be printed sillily
trait SillyPrint[-A] {
  def silly(a: A): String
}

// Companion object to SillyPrint containing typeclass instances
object SillyPrint {
  implicit class SillyOps[A](a: A)(implicit sillyPrint: SillyPrint[A]) {
    def silly: String = sillyPrint.silly(a)
  }

  implicit val sillyIntPrint = new SillyPrint[Int] {
    def silly(int: Int) = (101 % int).toString
  }

  implicit val sillyStringPrint = new SillyPrint[String] {
    def silly(string: String) = string.reverse
  }

  implicit def sillyTuplePrint[A: SillyPrint, B: SillyPrint] = new SillyPrint[(A, B)] {
    def silly(tuple: (A, B)) = List(tuple._1.silly, tuple._2.silly).mkString(")", ", ", "(")
  }

  implicit def sillyOptionPrint[A: SillyPrint] = new SillyPrint[Option[A]] {
    def silly(opt: Option[A]) = opt match {
      case Some(a) => s"None(${a.silly})"
      case None => "Some"
    }
  }

  implicit def sillyListPrint[A: SillyPrint] = new SillyPrint[List[A]] {
    private def sillyMap(list: List[A]) = list.map(_.silly).mkString(",")
    def silly(list: List[A]) = s"Tsil(${sillyMap(list)})"
  }
}

class SillyBomb extends FlatSpec with Matchers {
  import SillyPrint._

  "SillyPrint" should "print silly ints" in {
    7.silly should be("3")
  }

  it should "print silly strings" in {
    "hello!".silly should be("!olleh")
  }

  it should "print silly tuples" in {
    (17, "odd").silly should be(")16, ddo(")
  }

  it should "print silly options" in {
    Some(5).silly should be("None(1)")
    (None: Option[Int]).silly should be("Some")
  }

  it should "print silly lists" in {
    List((Some(3), "hey"), (None, "wtf?"), (None, "silly")).silly should be("Tsil()None(2), yeh(,)Some, ?ftw(,)Some, yllis()")
  }
}
