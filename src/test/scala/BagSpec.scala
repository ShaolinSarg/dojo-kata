package newcastle.dojo.kata

import scala.collection.immutable.HashMap
import org.scalatest.{ FlatSpec, Matchers }

class BagSpec() extends FlatSpec with Matchers {
  
  val bag = new Bag[Char](HashMap[Char, Int]('c' -> 2, 'd' -> 3))
  val bag1 = bag.add('e')
  val bag2 = bag.delete('c')

  "bag" should "hold multiple instances" in {
     bag.toString shouldBe "[Bag[c -> 2,d -> 3]]"
  }

  "bag" should "reveal how many instances" in {
     bag.numberOf('c') shouldBe 2
     bag.numberOf('d') shouldBe 3
     bag.numberOf('e') shouldBe 0
  }

  "bag1" should "hold multiple instances" in {
     bag1.numberOf('c') shouldBe 2
     bag1.numberOf('d') shouldBe 3
     bag1.numberOf('e') shouldBe 1
  }

  "bag2" should "hold multiple instances" in {
     bag2.numberOf('c') shouldBe 1
     bag2.numberOf('d') shouldBe 3
     bag2.numberOf('e') shouldBe 0
  }

}