package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }

class BinaryTree extends FlatSpec with Matchers {

  sealed abstract class BinaryTree[+T] {
    def addValue[U >: T <% Ordered[U]](elem: U): BinaryTree[U]
    def mkString(separator: String): String

    override def toString: String = mkString(", ")
  }

  case object EmptyTree extends BinaryTree[Nothing] {
    def addValue[U <% Ordered[U]](elem: U): BinaryTree[U] = Branch[U](elem, EmptyTree, EmptyTree)
    def mkString(separator: String): String = ""
  }

  case class Branch[+T](node: T, left: BinaryTree[T], right: BinaryTree[T]) extends BinaryTree[T] {
    def addValue[U >: T <% Ordered[U]](elem: U): BinaryTree[U] = {
      if ((elem compare node) < 0) Branch(node, left addValue elem, right)
      else if ((elem compare node) > 0) Branch(node, left, right addValue elem)
      else this
    }

    def mkString(separator: String): String = this match {
      case Branch(node, EmptyTree, EmptyTree) => node.toString
      case Branch(node, left, EmptyTree) => node + separator + (left mkString separator)
      case Branch(node, EmptyTree, right) => node + separator + (right mkString separator)
      case Branch(node, left, right) => node + separator + (left mkString separator) + separator + (right mkString separator)
    }
  }

  "toString" should "return nothing when applied to an empty tree" in {
    EmptyTree.toString shouldBe ""
  }
  it should "list the elements in pre-order when applied to a branch" in {
    Branch(6, EmptyTree, EmptyTree).toString shouldBe "6"
    Branch(5, Branch(6, EmptyTree, EmptyTree), Branch(7, EmptyTree, EmptyTree)).toString shouldBe "5, 6, 7"
  }

  "addValue" should "respect the arrangement of the tree" in {
    EmptyTree
      .addValue(6)
      .addValue(1)
      .addValue(3)
      .toString shouldBe "6, 1, 3"
    EmptyTree
      .addValue(6)
      .addValue(7)
      .addValue(5)
      .toString shouldBe "6, 5, 7"
  }
}