package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }

class NatParserSpec extends FlatSpec with Matchers {

  // This pattern is called the algebraic data type pattern -
  // we construct a type that holds all of the possible values in our syntax tree.
  //
  // This means that we can write functions that match over GrammarRule and
  // the compiler will tell us if we have a non-exhaustive match clause.
  trait GrammarRule;
  trait Sentence extends GrammarRule;
  case class Sentence1(vp: VerbPhrase, np: NounPhrase) extends Sentence;
  case class Sentence2(vp: VerbPhrase, pp: PrepositionalPhrase) extends Sentence;
  case class VerbPhrase(np: NounPhrase, v: Verb) extends GrammarRule;
  case class PrepositionalPhrase(p: Preposition, np: NounPhrase) extends GrammarRule;
  trait NounPhrase extends GrammarRule;
  case class NounPhrase1(d: Determiner, n: Noun) extends NounPhrase;
  case class NounPhrase2(d: Determiner, ap: AdjectivePhrase) extends NounPhrase;
  case class AdjectivePhrase(a: Adjective, ap: Noun) extends GrammarRule;
  trait Word extends GrammarRule;
  class Noun extends Word;
  class Verb extends Word;
  class Adjective extends Word;
  class Preposition extends Word;
  class Determiner extends Word;

  // Our dictionary
  object Fox extends Noun;
  object Dog extends Noun;
  object Jumps extends Verb;
  object Quick extends Adjective;
  object Brown extends Adjective;
  object Lazy extends Adjective;
  object Over extends Preposition;
  object The extends Determiner;

  def parse(sentence: List[GrammarRule]): Sentence = {

    // Our matches just need to be the same shape as our algebraic data type's data constructors -
    // the fields in the case classes. We pull these out by destructuring the list using pattern matching
    // and return the same list with the values we matched wrapped in their enclosing grammatical structure.
    def reduce(s: List[GrammarRule]): List[GrammarRule] = s match {
      case (a: Adjective) :: (n: Noun) :: tail => AdjectivePhrase(a, n) :: tail
      case (d: Determiner) :: (n: Noun) :: tail => NounPhrase1(d, n) :: tail
      case (d: Determiner) :: (ap: AdjectivePhrase) :: tail => NounPhrase2(d, ap) :: tail
      case (p: Preposition) :: (np: NounPhrase) :: tail => PrepositionalPhrase(p, np) :: tail
      case (np: NounPhrase) :: (v: Verb) :: tail => VerbPhrase(np, v) :: tail
      case (vp: VerbPhrase) :: (np: NounPhrase) :: tail => Sentence1(vp, np) :: tail
      case (vp: VerbPhrase) :: (pp: PrepositionalPhrase) :: tail => Sentence2(vp, pp) :: tail
      case _ => s
    }

    def shiftReduce(buffer: List[GrammarRule], sub: List[GrammarRule]): List[GrammarRule] = reduce(sub) match {
      // When our list is empty we return the buffer of matched rules
      case end if end.isEmpty => buffer
      // If the value we get out of reduce is the same as the one we put in we need to advance
      // - we pop one value off the input and put it into the buffer and try to match again
      case shift if shift == sub => shiftReduce(buffer :+ sub.head, sub.tail)
      // If we successfully reduced the input, we can start again from the beginning matching over
      // the new structures in the input
      case other => shiftReduce(List(), buffer ++ other)
    }

    shiftReduce(Nil, sentence) match {
      case (h: Sentence) :: Nil => h
      case _ => throw new Exception("Sentence is syntactically invalid.")
    }
  }

  // Scalatest
  "parse" should " return a syntax tree for a sentence." in {
    // The fox is not brown to keep things simpler :-)
    val s = List(The, Quick, Fox, Jumps, Over, The, Lazy, Dog);
    val tree = Sentence2(
      VerbPhrase(
        NounPhrase2(
          The,
          AdjectivePhrase(
            Quick,
            Fox
          )
        ),
        Jumps
      ),
      PrepositionalPhrase(
        Over,
        NounPhrase2(
          The,
          AdjectivePhrase(
            Lazy,
            Dog
          )
        )
      )
    );

    parse(s) shouldEqual tree;
  }

}