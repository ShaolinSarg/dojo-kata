package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }

class PetsSpec extends FlatSpec with Matchers {
  val pets = List("cat", "dog", "alligator")

  def getPetsAtIndices(indices: Seq[Int]): Seq[String] =
    for { i <- indices; if (i >= 0 && i < pets.size) } yield pets(i)

  //alternative solution
  //def getPetsAtIndices(indices : Seq[Int]) : Seq[String] = {
  //indices collect pets
  //}

  "The pet at 0" should "be cat" in {
    getPetsAtIndices(Seq(0)) shouldBe Seq("cat")
  }
  "The pet at 1" should "be dog" in {
    getPetsAtIndices(Seq(1)) shouldBe Seq("dog")
  }
  "The pet at 2" should "be alligator" in {
    getPetsAtIndices(Seq(2)) shouldBe Seq("alligator")
  }
  "The pets at 0, 42 and 2" should "be cat and alligator" in {
    getPetsAtIndices(Seq(0, 42, 2)) shouldBe Seq("cat", "alligator")
  }
  "The pets at 0, -1 and 1" should "be cat and dog" in {
    getPetsAtIndices(Seq(0, -1, 1)) shouldBe Seq("cat", "dog")
  }
}