package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }
import org.scalatest.prop.PropertyChecks
import org.scalacheck.Gen

class Game {

  import Game._

  type Row = List[Char]
  type Board = List[Row]

  val width = 8
  val height = 8
  val row: Row = List.fill(width)('0')
  var board: Board = List.fill(height)(row)
  def print = {
    board.map(innerList => innerList.mkString).mkString("\r\n")
  }
  def newGame: Game = {
    val topline = List('R', 'N', 'B', 'K', 'Q', 'B', 'N', 'R')
    val topline2 = List('P', 'P', 'P', 'P', 'P', 'P', 'P', 'P')
    val bottomline = List('r', 'n', 'b', 'k', 'q', 'b', 'n', 'r')
    val bottomline2 = List('p', 'p', 'p', 'p', 'p', 'p', 'p', 'p')
    board = topline :: topline2 :: row :: row :: row :: row :: bottomline2 :: bottomline :: Nil
    this
  }

  def place(piece: Piece, row: Int, col: Int) = {
    val updatedRow: Row = board(row - 1).patch(col - 1, List(piece.char), 1)
    val updatedBoard: Board = board.patch(row - 1, List(updatedRow), 1)
    board = updatedBoard
  }
}

object Game {
  sealed trait Piece {
    def char: Char = this match {
      case WhiteRook => 'R'
      case WhiteBishop => 'B'
    }
  }
  case object WhiteRook extends Piece
  case object WhiteBishop extends Piece
}

class ChessSpec extends FlatSpec with Matchers with PropertyChecks {

  import Game._

  "Chessboard" should "be 8*8" in {
    val board = new Game
    board.width shouldBe 8
    board.height shouldBe 8
  }

  it should "return a printed board as string" in {
    val board = new Game
    board.print shouldBe
      """00000000
    |00000000
    |00000000
    |00000000
    |00000000
    |00000000
    |00000000
    |00000000""".stripMargin
  }

  it should "return a new game board as string" in {
    val board = new Game
    board.newGame.print shouldBe
      """RNBKQBNR
    |PPPPPPPP
    |00000000
    |00000000
    |00000000
    |00000000
    |pppppppp
    |rnbkqbnr""".stripMargin
  }

  it should "place a rook at space (1,1)" in {
    val board = new Game
    board.place(WhiteRook, 1, 1)
    board.print shouldBe
      """R0000000
    |00000000
    |00000000
    |00000000
    |00000000
    |00000000
    |00000000
    |00000000""".stripMargin
  }

  it should "place a bishop at space (1,3)" in {
    val board = new Game
    board.place(WhiteBishop, 1, 3)

    board.print shouldBe
      """00B00000
    |00000000
    |00000000
    |00000000
    |00000000
    |00000000
    |00000000
    |00000000""".stripMargin
  }
}