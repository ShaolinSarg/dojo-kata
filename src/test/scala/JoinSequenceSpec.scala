package newcastle.dojo.kata

import org.scalatest.{ FunSpec, Matchers }

class JoinSequenceSpec() extends FunSpec with Matchers {

  def join(inputList1: List[Int], inputList2: List[Int]): List[Int] = inputList1 ::: inputList2

  val numberList1: List[Int] = List(1, 1, 2, 3, 5, 8)
  val numberList2: List[Int] = List(6, 4, 7)

  describe("join") {
    describe("given 2 lists") {
      it("should return 1 list with the contents of both lists") {
        join(numberList1, numberList2) shouldBe List(1, 1, 2, 3, 5, 8, 6, 4, 7)
      }
    }
  }

}