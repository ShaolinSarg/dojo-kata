package newcastle.dojo.kata

import org.scalatest.{ FunSpec, Matchers }

class GreySpec extends FunSpec with Matchers {

  def shadesOfGrey(n: Int): Seq[String] = {
    if (n <= 0) Seq()
    else {
      val r = if (n > 254) 254 else n

      for {
        i <- (1 to r)
        x = (1 to 3).map(_ => "%02x".format(i)).mkString("")
      } yield "#" + x
    }
  }

  describe("converts number to colour code") {
    describe("given a number") {
      it("it should return a list of hex colour codes for 1") {
        shadesOfGrey(1) shouldBe Seq("#010101")
      }
      it("it should return a hex colour code 0f for 10") {
        shadesOfGrey(10) shouldBe Seq("#010101", "#020202", "#030303", "#040404", "#050505", "#060606", "#070707", "#080808", "#090909", "#0a0a0a")
      }
    }

    describe("given an out of range number") {
      it("it should return empty list for 0") {
        shadesOfGrey(0) shouldBe (Seq())
      }
      it("it should return empty list for -1") {
        shadesOfGrey(0) shouldBe (Seq())
      }
      it("it should have a size of 254 for numbers above 254") {
        shadesOfGrey(256) should have size 254
      }
      it("it should have a the same elements as 254 for numbers above 254") {
        shadesOfGrey(256) shouldBe shadesOfGrey(254)
      }
    }

  }
}