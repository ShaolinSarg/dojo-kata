package newcastle.dojo.kata

import scala.util.Random
import org.scalatest.{ FlatSpec, Matchers }

class TriggerSpec extends FlatSpec with Matchers {

  object Generators {

    def range(low: Int, high: Int): Stream[Int] = Stream.continually(Random.nextInt(high - low) + low)

  }

  "The trigger" should "return a time between 180 and 4320 minutes" in {
    val randomRange = Generators.range(180, 4320)

    randomRange.take(100).foreach { number =>
      number shouldBe 2250 +- 2070
    }

  }

  it should "return the same 100 numbers when called twice" in {
    val randomRange = Generators.range(180, 4320)
    randomRange.take(100) shouldBe randomRange.take(100)
  }

}