package newcastle.dojo.kata

import org.scalatest.Matchers
import org.scalatest.FlatSpec
import scala.annotation.tailrec

class InterleaveBomb extends FlatSpec with Matchers {
  /*def interleave[T](xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
    case (xhead :: xtail, yhead :: ytail) => xhead :: yhead :: interleave(xtail, ytail)
    case _ => Nil
  }*/

  def interleave[T](xs: List[T], ys: List[T]): List[T] = {
    @tailrec
    def innerInterLeave(xs: List[T], ys: List[T], acc: List[T]): List[T] = (xs, ys) match {
      case (xhead :: xtail, yhead :: ytail) => innerInterLeave(xtail, ytail, acc ++ List[T](xhead, yhead))
      case _ => acc
    }
    innerInterLeave(xs, ys, Nil)
  }

  /* As per Brendan's comment, it's often more efficient to build a list backwards and reverse it */
  def interleaveReverse[T](xs: List[T], ys: List[T]): List[T] = {
    @tailrec
    def innerInterLeave(xs: List[T], ys: List[T], acc: List[T]): List[T] = (xs, ys) match {
      case (xhead :: xtail, yhead :: ytail) => innerInterLeave(xtail, ytail, yhead :: xhead :: acc)
      case _ => acc.reverse
    }
    innerInterLeave(xs, ys, Nil)
  }

  "interleave" should "return empty list when either list is empty" in {
    interleave(Nil, Nil) should be(Nil)
    interleave(List(0), Nil) should be(Nil)
    interleave(Nil, List(0)) should be(Nil)
  }

  it should "interleave the elements" in {
    interleave(List("a", "c", "e"), List("b", "d", "f")) should be(List("a", "b", "c", "d", "e", "f"))
  }

  it should "stop when either list is empty" in {
    interleave(List(1, 3), List(2, 4, 6)) should be(List(1, 2, 3, 4))
    interleave(List(1, 3, 5), List(2, 4)) should be(List(1, 2, 3, 4))
  }
}
