package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }

class UFOSpec extends FlatSpec with Matchers {
  val comet = "A"
  val group = "B"
  val group1 = "COMETQ"
  val comet1 = "HVNGAT"

  val go = "GO"
  val stay = "STAY"
  val base = 'A'.toInt - 1

  def getMod47Value(str: String): Int = str.map(x => { x.toInt - base }).product % 47

  def ride(group: String, comet: String): String = {
    if (getMod47Value(group) == getMod47Value(comet)) go
    else stay
  }

  "ride" should "return GO for comet and group with same name" in {
    ride(group, group) shouldBe go
    ride(comet, comet) shouldBe go
    ride(group1, group1) shouldBe go
    ride(comet1, comet1) shouldBe go
  }
  it should "return STAY for most different names" in {
    ride(group, comet) shouldBe stay
    ride(comet, group) shouldBe stay
    ride(group, group1) shouldBe stay
    ride(group1, group) shouldBe stay
    ride(comet, comet1) shouldBe stay
    ride(comet1, comet) shouldBe stay
  }
  it should "except those which are mod equivalent" in {
    ride(group1, comet1) shouldBe go
    ride(comet1, group1) shouldBe go
  }
}