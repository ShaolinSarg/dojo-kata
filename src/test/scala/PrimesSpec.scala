package newcastle.dojo.kata

import org.scalatest.{FlatSpec,Matchers}

class PrimesSpec() extends FlatSpec with Matchers {
  val primes: Stream[Int] = 2 #:: Stream.from(3, 2).filter(sieve)
  def sieve(i: Int) : Boolean =
   primes.takeWhile(_ <= Math.sqrt(i)).forall(i % _ != 0) 
  
  "the first prime number" should "be 2" in {
    primes.take(1).head shouldBe 2
  }

  "the second prime number" should "be 3" in {
    primes.take(2).reverse.head shouldBe 3
  }

  "the third prime number" should "be 5" in {
    primes.take(3).reverse.head shouldBe 5
  }

  "the fourth prime number" should "be 7" in {
    primes.take(4).reverse.head shouldBe 7
  }

  "the first 10 prime numbers" should "be 2,3,5,7,11,13,17,19,23,29" in {
    primes.take(10) shouldBe List(2,3,5,7,11,13,17,19,23,29)
  }

  "the 10th prime number" should "be 29" in {
    primes.take(10).reverse.head shouldBe 29
  }

  "the 100th prime number" should "be 541" in {
    primes.take(100).reverse.head shouldBe 541
  }

  "the 1000th prime number" should "be 7919" in {
    primes.take(1000).reverse.head shouldBe 7919
  }

}