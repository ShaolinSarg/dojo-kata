package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }
import java.io.ByteArrayOutputStream

class QuineSpec() extends FlatSpec with Matchers {
  
  val Quine1Source = "object Quine1 extends App{val s=\"object Quine1 extends App{val s=%c%s%c;printf(s,34,s,34)}\";printf(s,34,s,34)}"
  val Quine2Source = "object Quine2 extends App{val s=\"object Quine2 extends App{val s=%c%s%1$c;printf(s,34,s)}\";printf(s,34,s)}"
  val ReverseQuineSource = "object ReverseQuine extends App{val s=\"object ReverseQuine extends App{val s=c%s%c%;printf(s.reverse,34,s.reverse,34)}\";printf(s.reverse,34,s.reverse,34)}"
  
  "Quine1" should "print itself out" in {
    val stream = new ByteArrayOutputStream()
    Console.withOut(stream) {
      Quine1.main(Array())
    }
    stream.toString() shouldBe Quine1Source
  }

  "Quine2" should "also print itself out" in {
    val stream = new ByteArrayOutputStream()
    Console.withOut(stream) {
      Quine2.main(Array())
    }
    stream.toString() shouldBe Quine2Source
  }

  "ReverseQuine" should "print itself out backwards" in {
    val stream = new ByteArrayOutputStream()
    Console.withOut(stream) {
      ReverseQuine.main(Array())
    }
    stream.toString() shouldBe ReverseQuineSource.reverse
  }

}

  object Quine1 extends App{val s="object Quine1 extends App{val s=%c%s%c;printf(s,34,s,34)}";printf(s,34,s,34)}

  object Quine2 extends App{val s="object Quine2 extends App{val s=%c%s%1$c;printf(s,34,s)}";printf(s,34,s)}

  object ReverseQuine extends App{val s="object ReverseQuine extends App{val s=c%s%c%;printf(s.reverse,34,s.reverse,34)}";printf(s.reverse,34,s.reverse,34)}
