package newcastle.dojo.kata

import org.scalatest.{FlatSpec, Matchers}
import System.lineSeparator 

class ShoppingBasket extends FlatSpec with Matchers{

  def shoppingBasket(items: List[String]) = {
    val baskets = items.inits.toList.reverse.tail
    baskets.map(basketToString).mkString(lineSeparator)
  }

  def basketToString(items: List[String]): String = {
    def joinItems(items: List[String]): String = items match {
      case Nil => ""
      case item :: Nil => item
      case item1 :: item2 :: Nil => item1 + ", and " + item2
      case item :: moreItems => item + ", " + joinItems(moreItems)
    }

    "In my basket I have " + joinItems(items) + "."
  }


  "shoppingBasket" should "return every line of the memory game" in {
   
    val items1 = List("a pineapple", "some juice", "a melon", "a pair of shoes", "a magazine", "some yoghurt")
                            
    val basket1 = """|In my basket I have a pineapple.
                     |In my basket I have a pineapple, and some juice.
                     |In my basket I have a pineapple, some juice, and a melon.
                     |In my basket I have a pineapple, some juice, a melon, and a pair of shoes.
                     |In my basket I have a pineapple, some juice, a melon, a pair of shoes, and a magazine.
                     |In my basket I have a pineapple, some juice, a melon, a pair of shoes, a magazine, and some yoghurt.""".trim.stripMargin
    val items2 = List("an apple", "a hat", "a bunch of bananas", "a Dizzee Rascal album", "Titanic on VHS",
                    "a new phone","a bottle of wine","some other stuff")                        
    val basket2 = """|In my basket I have an apple.
                     |In my basket I have an apple, and a hat.
                     |In my basket I have an apple, a hat, and a bunch of bananas.
                     |In my basket I have an apple, a hat, a bunch of bananas, and a Dizzee Rascal album.
                     |In my basket I have an apple, a hat, a bunch of bananas, a Dizzee Rascal album, and Titanic on VHS.
                     |In my basket I have an apple, a hat, a bunch of bananas, a Dizzee Rascal album, Titanic on VHS, and a new phone.
                     |In my basket I have an apple, a hat, a bunch of bananas, a Dizzee Rascal album, Titanic on VHS, a new phone, and a bottle of wine.
                     |In my basket I have an apple, a hat, a bunch of bananas, a Dizzee Rascal album, Titanic on VHS, a new phone, a bottle of wine, and some other stuff.""".trim.stripMargin
    shoppingBasket(items1) shouldBe basket1
    shoppingBasket(items2) shouldBe basket2
  }
}