package newcastle.dojo.kata

import org.scalatest.{FlatSpec, Matchers}

class HigherSpec extends FlatSpec with Matchers{

  def processPair(f: (Int, Int) => Int) = (t: (Int, Int)) => f(t._1, t._2)

 "passing a function" should "return a function that can operate on a pair" in {

   processPair(_ * _)((6,7)) shouldBe 42
   (1 to 2) zip (9 to 7 by -1) map processPair(_ + _) shouldBe Vector(10,10)
 }

}