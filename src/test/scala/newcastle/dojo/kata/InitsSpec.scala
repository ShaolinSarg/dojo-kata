package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }

class InitsSpec extends FlatSpec with Matchers {
  def myInits[A](list: List[A]): List[List[A]] = {
    def makeSubList[A](list: List[A], acc: List[List[A]]) : List[List[A]] = list.length match {
      case 0 => acc.reverse ::: List(Nil)
      case _ => makeSubList[A](list.slice(0, list.length - 1), (list :: acc))
    }
    makeSubList[A](list, Nil)
  }

  "myInits" should "return list containing empty list when used with an empty list" in {
    myInits(Nil) shouldBe List(Nil)
  }

  "myInits" should "return list containing 2 lists when used with single item list" in {
    myInits(List(1)) shouldBe List(List(1), Nil)
  }

  "myInits" should "return list containing 3 lists when used with 2 item list" in {
    myInits(List("a", "b")) shouldBe List(List("a", "b"), List("a"), Nil)
  }

  "myInits" should "return list containing n+1 lists when used with n item list" in {
    myInits((1 to 20).toList) shouldBe (1 to 20).inits.toList
  }
}