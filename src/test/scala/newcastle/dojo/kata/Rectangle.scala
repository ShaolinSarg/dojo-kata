package newcastle.dojo.kata

import org.scalatest.FlatSpec
import org.scalatest.Matchers

class Rectangle  extends FlatSpec with Matchers {
 def numberOfRectangles(height :Int, width: Int) = {
   height*(height+1)*width*(width+1)/4
 }


 "number of rectanges" should "match given results" in {
   numberOfRectangles(3, 2) shouldEqual 18
   numberOfRectangles(4, 4) shouldEqual 100
 }

} 