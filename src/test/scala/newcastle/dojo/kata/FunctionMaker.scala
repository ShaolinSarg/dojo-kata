package newcastle.dojo.kata

import org.scalatest.FlatSpec
import org.scalatest.Matchers

class FunctionMaker extends FlatSpec with Matchers {
  
  val s1 = Set(1)
  val s2 = Set(1, 2)
  val s3 = Set(1, 2, 3)
  
  def uniqueBinaryFunctions[T](s: Set[T]): Set[Map[(T, T), T]] = {
    val inputPairs = for {
        a <- s
        b <- s
    } yield (a, b)
    
    def cumulativeMapsFromPairs(pairs: Set[(T, T)]): Set[Map[(T, T), T]] = {
      if (pairs.isEmpty) Set(Map[(T, T), T]())
      else for {
        elem <- s
        maps <- cumulativeMapsFromPairs(pairs.tail)
      } yield maps.updated(pairs.head, elem)
    }
    cumulativeMapsFromPairs(inputPairs)
  }
  
  "Unique binary functions over a set of size 1" should "total 1" in {
    uniqueBinaryFunctions(s1).size shouldBe (1)
  }
  it should "include the function (1, 1) -> 1" in {
    uniqueBinaryFunctions(s1) should contain (Map((1, 1) -> 1))
  }
  
  "Unique binary functions over a set of size 2" should "total 16" in {
    uniqueBinaryFunctions(s2).size shouldBe (16)
  }
  it should "include the function (1, 1) -> 1" in {
    uniqueBinaryFunctions(s2) should contain (Map((1, 1) -> 1,
                                                  (1, 2) -> 2,
                                                  (2, 1) -> 1,
                                                  (2, 2) -> 1))
  }
  
  "Unique binary functions over a set of size 3" should "total 19683" in {
    uniqueBinaryFunctions(s3).size shouldBe (19683)
  }
  
}