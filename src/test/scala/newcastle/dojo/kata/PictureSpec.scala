package newcastle.dojo.kata

import org.scalatest.{FlatSpec,Matchers}
import System.lineSeparator

class PictureSpec extends FlatSpec with Matchers {
  
  def mirror(picture: String): String = {
    val lines = picture.split(lineSeparator)
    val maxLength = lines.map(a => a.length()).max
    val paddedLines = lines.map(a => a.padTo(maxLength," ").mkString(""))
    val outputLines = paddedLines.map(a => (a + a.reverse))
    outputLines.mkString(lineSeparator)
  }
  
  val halfButterfly = """| ##  
                         | #@# 
                         |  #@#
                         |   ##
                         |   ##
                         |  #  
                         | #   """.stripMargin
                     
 
  val fullButterfly = """| ##    ## 
                         | #@#  #@# 
                         |  #@##@#  
                         |   ####   
                         |   ####   
                         |  #    #  
                         | #      # """.stripMargin
                
  
  "mirror" should "see a beautiful butterfly emerge!" in {
    mirror(halfButterfly) shouldBe fullButterfly
  }   
  
  
}