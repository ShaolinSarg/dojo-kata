package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }

class TextSpeakify extends FlatSpec with Matchers {
  /* In a bid to seem more approachable to teenagers of today, write a function which returns 
   * a text message sans vowels. And end each text in 'm8' for good measure */
   
  def textSpeakify(string: String): String = {
    def removeVowels(string: String): String = {
      string.replaceAll("[A,E,I,O,U,a,e,i,o,u]", "")
    }
    removeVowels(string) + " m8"
  }

  
  val test1 = "The rain falls mainly on the plain"
  val outcome1 = "Th rn flls mnly n th pln m8"
  val test2 = "Make sure you're home for 8. We're having fish fingers"
  val outcome2 = "Mk sr y'r hm fr 8. W'r hvng fsh fngrs m8"
  val test3 = "But what are the effects on pack printing"  
  val outcome3 = "Bt wht r th ffcts n pck prntng m8"
  
  "textSpeakify" should "remove all vowels from a string to make it less legible" in {
    textSpeakify(test1) shouldBe outcome1
    textSpeakify(test2) shouldBe outcome2
    textSpeakify(test3) shouldBe outcome3
  }
}