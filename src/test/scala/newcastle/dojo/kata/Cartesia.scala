package newcastle.dojo.kata

import org.scalatest.{FlatSpec, Matchers}

class Cartesia extends FlatSpec with Matchers {
  
  def goodWalk(appWalk: List[Char]): Boolean = {
    if (appWalk.length != 10) false
    else {
      val eastShift = appWalk.count(_ == 'e') - appWalk.count(_ == 'w')
      val northShift = appWalk.count(_ == 'n') - appWalk.count(_ == 's')
      northShift == 0 && eastShift == 0
    }
  }
  
  "good walk" should "be true for a 10-block rectangle" in (
    goodWalk(List('n','n','n','e','e','s','s','s','w','w')) shouldBe true    
  )
  it should "be false for a smaller rectangle" in (
    goodWalk(List('n','n','e','s','s','w')) shouldBe false
  )
  it should "be false for a walk 10 blocks north" in (
    goodWalk(List('n','n','n','n','n','n','n','n','n','n')) shouldBe false
  )
  it should "be false for staying where you are" in (
    goodWalk(List()) shouldBe false   
  )
  it should "be true for an 8-block figure-8 followed by a south and a north" in (
    goodWalk(List('n','n','e','s','w','w','s','e','s','n')) shouldBe true
  )
}