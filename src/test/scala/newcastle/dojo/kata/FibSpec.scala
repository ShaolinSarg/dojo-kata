package newcastle.dojo.kata

import org.scalatest.{FlatSpec, Matchers}

class FibSpec extends FlatSpec with Matchers {
 def fibonacciBuilder(a: Long, b: Long): Stream[Long] = {
   Stream.iterate( (a,b) ) { case (a,b) => (b,a+b)}.map(_._1)
 }
 //val fibs: Stream[Long] = 0L #:: fibs.scanLeft(1L)(_ + _)
 val fibs: Stream[Long] = fibonacciBuilder(0L, 1L)

 "the first fibonacci number" should "be correct" in {
   fibs.take(1).head shouldBe 0
 }

 "the second fibonacci number" should "be correct" in {
   fibs.take(2).reverse.head shouldBe 1
 }

 "the third fibonacci number" should "be correct" in {
   fibs.take(3).reverse.head shouldBe 1
 }

 "the fourth fibonacci number" should "be correct" in {
   fibs.take(4).reverse.head shouldBe 2
 }

 "the first 10 fibonacci numbers" should "be correct" in {
   fibs.take(10) shouldBe List(0, 1, 1, 2, 3, 5, 8, 13, 21, 34)
 }

 "the 10th fibonacci number" should "be correct" in {
   fibs.take(10).reverse.head shouldBe 34
 }

 "the 50th fibonacci number" should "be correct" in {
   fibs.take(50).reverse.head shouldBe 7778742049L
 }

}