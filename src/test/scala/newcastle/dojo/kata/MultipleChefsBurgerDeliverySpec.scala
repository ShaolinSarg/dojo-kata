package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }

class MultipleChefsBurgerDeliverySpec extends FlatSpec with Matchers {
  /* burgerPlan takes a list of tuples consisting of the order number, time of placing (in magic time units), and
     time to cook (in magic time units) and returns a list of orders and their completion time, sorted by completion time
   */
  def burgerPlan(orders: List[(Int, Int, Int)], numberOfChefs: Int, chefs: List[(Int, Int)] = List()): List[(Int, Int)] = {
     
    val chefList: List[(Int,Int)] = {  
      if (chefs.isEmpty) 
        (for (i <- 1 to numberOfChefs) yield ((i, 0))).sortBy(_._2).toList
      else chefs.sortBy(_._2)
    }
    
    val nextOrderList = orders.sortBy(_._2)
    val newFinish = {
      if (chefList.head._2 != 0) chefList.head._2 + nextOrderList.head._3 
      else nextOrderList.head._2 + nextOrderList.head._3 
    }
    val newChefList = (chefList.head._1, newFinish) :: chefList.tail
    
    nextOrderList match {
      case (a, b, c) :: Nil => List((a, (newFinish)))
      case (a, b, c) :: tail => (List((a, newFinish)) ::: burgerPlan(tail, numberOfChefs, newChefList)).sortBy(_._2)
    }
    
  }

  /* each chef can only cook one order at a time, because they are rubbish */

  val testOrderList = List((1, 1, 10), (2, 2, 3), (3, 6, 7), (4, 5, 1))

  val expectedOrderDelivery = List((2, 5), (4, 6), (1, 11), (3, 13))

  val expectedOrderDeliveryOneChef = List((1, 11), (2, 14), (4, 15), (3, 22))

  "burgerPlanning" should "come up with an efficient order for two chefs" in {
    burgerPlan(testOrderList, 2) shouldBe expectedOrderDelivery
  }

  "burgerPlanning" should "come up with an efficient order for one chef" in {
    burgerPlan(testOrderList, 1) shouldBe expectedOrderDeliveryOneChef
  }

}
