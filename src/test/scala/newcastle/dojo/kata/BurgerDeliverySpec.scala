package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }

/**
 * Created by MackayR on 14/08/2015.
 */
class BurgerDeliverySpec extends FlatSpec with Matchers {
  /* burgerPlan takes a list of tuples consisting of the order number, time of placing (in magic time units), and
     time to cook (in magic time units) and returns a list of orders and their completion time, sorted by completion time
   */
  def burgerPlan(orders: List[(Int, Int, Int)]): List[(Int, Int)] = {
    def planBurgers(chef1FreeTime: Int, chef2FreeTime: Int, orderNumber: Int,
      startTime: Int, duration: Int, remainingOrders: List[(Int, Int, Int)],
      acc: List[(Int, Int)]): List[(Int, Int)] = remainingOrders match {
      case head :: tail =>
        val times = calculateFinishTime(chef1FreeTime, chef2FreeTime, startTime, duration)
        planBurgers(times._1, times._2, head._1, head._2, head._3, tail, (orderNumber, times._3) :: acc)
      case head :: Nil =>
        val times = calculateFinishTime(chef1FreeTime, chef2FreeTime, startTime, duration)
        planBurgers(times._1, times._2, head._1, head._2, head._3, Nil, (orderNumber, times._3) :: acc)
      case Nil => (orderNumber, calculateFinishTime(chef1FreeTime, chef2FreeTime, startTime, duration)._3) :: acc
    }
    val sortedOrders = orders.sortBy(_._2)
    planBurgers(0, 0, sortedOrders.head._1, sortedOrders.head._2, sortedOrders.head._3, sortedOrders.tail, Nil).sortBy(_._2)
  }

  def calculateFinishTime(chef1FreeTime: Int, chef2FreeTime: Int, startTime: Int, duration: Int): (Int, Int, Int) = {
    if (chef1FreeTime <= chef2FreeTime) {
      if (startTime > chef1FreeTime) (startTime + duration, chef2FreeTime, startTime + duration) else (chef1FreeTime + duration, chef2FreeTime, chef1FreeTime + duration)
    } else {
      if (startTime > chef2FreeTime) (chef1FreeTime, startTime + duration, startTime + duration) else (chef1FreeTime, chef2FreeTime + duration, chef2FreeTime + duration)
    }
  }

  /* each chef can only cook one order at a time, because they are rubbish */
  val numberOfChefs = 2

  val testOrderList = List((1, 1, 10), (2, 2, 3), (3, 6, 7), (4, 5, 1))

  val expectedOrderDelivery = List((2, 5), (4, 6), (1, 11), (3, 13))

  "burgerPlanning" should "come up with an efficient order" in {
    burgerPlan(testOrderList) shouldBe expectedOrderDelivery
  }

}
