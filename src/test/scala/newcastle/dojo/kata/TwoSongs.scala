package newcastle.dojo.kata

import org.scalatest.{FlatSpec,Matchers}

class TwoSongs extends FlatSpec with Matchers {
  def songmix (lists : List[String]): String = {
    (lists.head :: lists.tail.map(_.toLowerCase)).mkString(" ")
  }
  
  
  val REM = List("Everybody", "Hurts")
  val Adele = List("Someone", "Like", "You")
  val Clash = List("London", "Calling")
  val Beatles = List("For", "No", "One")
  val Dylan = List("Tangled", "Up", "In", "Blue")
  val Nail = List("Crocodile", "Shoes")
  
  "REM plus Adele" should "be pretty aggressive" in {
    songmix(REM ::: Adele) shouldBe "Everybody hurts someone like you"
  }
  
  "Clash plus Beatles" should "be sad" in {
    songmix(Clash ::: Beatles) shouldBe "London calling for no one"
  }
  
  "Clash plus Adele" should "be interesting" in {
    songmix(Clash ::: Adele) shouldBe "London calling someone like you"
  }

  "Dylan plus Nail" should "be sartorially challenged" in {
    songmix(Dylan ::: Nail) shouldBe "Tangled up in blue crocodile shoes"
  }
}