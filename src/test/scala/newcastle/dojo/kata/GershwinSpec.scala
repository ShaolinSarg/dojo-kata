package newcastle.dojo.kata

import org.scalatest.{FlatSpec, Matchers}

trait saySomething {
  def saying = {
    "You say " + word
  }
  
  def word : String
}

trait respond extends saySomething {
  abstract override def saying = {
    super.saying.replace("You", "I")
  }
}

case class yousay(wd: String) extends saySomething {
  def word = wd
}

case class isay(wd: String) extends respond {
  def word = wd
} 

class GershwinSpec extends FlatSpec with Matchers {
  //implicit def convert(obj: yousay) : isay = new isay(obj.word)
   implicit def fromYouToMe(you: yousay): isay = you match {
   case yousay(wd: String) => isay(wd)
 } 
   
  val EITHER = "either"
  val NEITHER = "neither"
  val TOMATO = "tomato"
  val POTATO = "potato"
      
  val firstLine = yousay(EITHER)
  val secondLine = yousay(NEITHER)
  val thirdLine = yousay(TOMATO)
  val fourthLine = yousay(POTATO)
  
  //val firstLineResponse = new yousay(EITHER) with respond
  //val secondLineResponse = new yousay(NEITHER) with respond
  //val thirdLineResponse = new yousay(TOMATO) with respond
  //val fourthLineResponse = new yousay(POTATO) with respond
  
  //val firstLineResponse = new firstLine with respond
  //val secondLineResponse = new secondLine with respond
  //val thirdLineResponse = new thirdLine with respond
  //val fourthLineResponse = new fourthLine with respond

  val firstLineResponse : isay = firstLine
  val secondLineResponse : isay = secondLine
  val thirdLineResponse : isay = thirdLine
  val fourthLineResponse : isay = fourthLine

  "My first call" should "be either" in {
   firstLine.saying shouldBe "You say either"
  }

  "My second call" should "be neither" in {
   secondLine.saying shouldBe "You say neither"
  }

  "My third call" should "be tomato" in {
   thirdLine.saying shouldBe "You say tomato"
  }

  "My fourth call" should "be potato" in {
   fourthLine.saying shouldBe "You say potato"
  }

  "My first response" should "be either" in {
   firstLineResponse.saying shouldBe "I say either"
  }

  "My second response" should "be neither" in {
   secondLineResponse.saying shouldBe "I say neither"
  }

  "My third response" should "be tomato" in {
   thirdLineResponse.saying shouldBe "I say tomato"
  }

  "My fourth response" should "be potato" in {
   fourthLineResponse.saying shouldBe "I say potato"
  }
}