package newcastle.dojo.kata

import org.scalatest.{FlatSpec,Matchers}

class NumbersToWords extends FlatSpec with Matchers {
  
  def generateWord (int: Int): String = {
    
    def intToIncreasingSignificanceList(n: Int) = n.toString.split("").filter(_ != "").toList.map(_.toInt).reverse
    
    val reverseDigitList = intToIncreasingSignificanceList(int)
    
    def digitToWordList(digit: Int): List[String] = digit match {
      case 0 => Nil
      case 1 => List("One")
      case 2 => List("Two")
      case 3 => List("Three")
      case 4 => List("Four")
      case 5 => List("Five")
      case 6 => List("Six")
      case 7 => List("Seven")
      case 8 => List("Eight")
      case 9 => List("Nine")
      case _ => throw new Exception("Not a digit!")
    }
    
    def tensToWordList(digit: Int): List[String] = digit match {
      case 0 => Nil
      case 1 => List("Ten")
      case 2 => List("Twenty")
      case 3 => List("Thirty")
      case 4 => List("Forty")
      case 5 => List("Fifty")
      case 6 => List("Sixty")
      case 7 => List("Seventy")
      case 8 => List("Eighty")
      case 9 => List("Ninety")
      case _ => throw new Exception("Not a digit!")
    }
    
    def teensToWordList(digit: Int): List[String] = digit match {
      case 0 => List("Ten")
      case 1 => List("Eleven")
      case 2 => List("Twelve")
      case 3 => List("Thirteen")
      case 4 => List("Fourteen")
      case 5 => List("Fifteen")
      case 6 => List("Sixteen")
      case 7 => List("Seventeen")
      case 8 => List("Eighteen")
      case 9 => List("Nineteen")
      case _ => throw new Exception("Not a digit!")
    }
    
    
    def firstTwoDigitsWords(digits: List[Int]) = digits match {
      case Nil => List("Zero")
      case 0 :: Nil => List("Zero")
      case digit :: Nil => digitToWordList(digit)
      case 0 :: tens :: _ => tensToWordList(tens)
      case digit :: 1 :: _ => teensToWordList(digit)
      case digit :: tens :: _ =>  digitToWordList(digit) ::: tensToWordList(tens)
    }
    
    def firstXDigits(digits: List[Int], x: Int): List[String] = {
      
      def significance(x: Int) = x match {
        case 3 => List("Hundred")
        case 4 => List("Thousand")
      }
      
      def and(x: Int) = if (x == 3) List("and") else Nil
      
      if (x <= 2) firstTwoDigitsWords(digits)
      else {
        digits.drop(x-1) match {
          case Nil => firstXDigits(digits, x-1)
          case 0 :: _ => firstXDigits(digits, x-1)
          case n :: _ => firstXDigits(digits, x-1) ::: and(x) ::: significance(x) ::: digitToWordList(n)
        }
      }
    }
    
    firstXDigits(reverseDigitList, reverseDigitList.length).reverse.mkString(" ")
    
  }  
  
  "generateWord" should "convert 1738 to full words" in {
    generateWord(1738) shouldBe "One Thousand Seven Hundred and Thirty Eight"
  }
  
  it should "convert 922 to full words" in {
    generateWord(922) shouldBe "Nine Hundred and Twenty Two"
  }
  
  it should "convert 64 to full words" in {
    generateWord(64) shouldBe "Sixty Four"
  }
}