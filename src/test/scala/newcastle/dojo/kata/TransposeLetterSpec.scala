package newcastle.dojo.kata

import org.scalatest.{FlatSpec, Matchers}

class TransposeLetterSpec extends FlatSpec with Matchers{
  def transpose(name: String): String = name.grouped(2).map(cs => s"${cs.tail}${cs.head}").mkString
  
  "Strings of all the same letters" should "return same string" in {
    transpose("") shouldBe ""
    transpose("aaaaa") shouldBe "aaaaa"
  }
  
  "Strings of even length" should "simply swap pairs of letters" in {
    transpose("Graeme") shouldBe "rGeaem"
    transpose("Bill") shouldBe "iBll"
  }
  
  "Strings of odd length" should "have the same last letter" in {
    transpose("abc") shouldBe "bac"
    transpose("Michael") shouldBe "iMhceal"
  }
}