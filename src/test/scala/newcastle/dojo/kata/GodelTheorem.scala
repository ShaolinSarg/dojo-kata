package newcastle.dojo.kata

import org.scalatest.{ FunSuite, Matchers }

class GodelTheorem extends FunSuite with Matchers {
  /* We have a simple mathematical model, containing one number: 0, and two
  functions - the successor, and the predecessor. There are no negative numbers.
  Using these three starting points, can we define addition, subtraction, and
  multiplication of two numbers using recursive methods?
   */
  def s(x: Int): Int = x + 1

  def p(x: Int): Int = {
    if (x == 0) 0
    else x - 1
  }

  def addition(x: Int, y: Int): Int = {
    if (y == 0) x
    else addition(s(x), p(y))
  }

  def multiplication(x: Int, y: Int): Int = {
    if (y == 0) 0
    else addition(x, multiplication(x, p(y)))
  }

  def subtraction(x: Int, y: Int): Int = {
    if (y == 0) x
    else subtraction(p(x), p(y))
  }

  def power(x: Int, y: Int): Int = {
    if (y == 0) s(0)
    else if (x == 0) 0
    else multiplication(x, power(x, p(y)))
  }

  def lessThanEqual(x: Int, y: Int): Boolean = {
    if (x == 0) true
    else if (y == 0) false
    else lessThanEqual(p(x), p(y))
  }

  def min(x: Int, y: Int): Int = if (lessThanEqual(x, y)) x else y
  
  def quotient(x: Int, y: Int): Int = {
    if (y == 0) throw new IllegalArgumentException
    else if (lessThanEqual(y,x)) s(quotient(subtraction(x,y),y))
    else 0
  }
  
  def remainder(x: Int, y: Int): Int = {
    if (y == 0) throw new IllegalArgumentException
    else if (lessThanEqual(y,x)) remainder(subtraction(x,y),y)
    else x
  }
  
  def gcd(x: Int, y: Int): Int = {
    def gcdTry(x: Int, y: Int, toTry: Int): Int = {
      if (toTry == 0) 0
      else if (remainder(x, toTry) == 0 && remainder(y, toTry) == 0) toTry
      else gcdTry(x, y, p(toTry))
    }
    gcdTry(x, y, min(x, y))
  }
  
  def tetration(x: Int, y: Int): Int = {
    if (x == 0 || y == 0) s(0)
    else power(x, tetration(x, p(y)))
  }

  test("addition of two numbers is correct") {
    addition(1, 2) shouldBe 3
    addition(4, 3) shouldBe 7
  }

  test("multiplication of two numbers is correct") {
    multiplication(0, 5) shouldBe 0
    multiplication(3, 1) shouldBe 3
    multiplication(2, 2) shouldBe 4
  }

  test("subtraction of two numbers is correct") {
    subtraction(9, 3) shouldBe 6
    subtraction(5, 0) shouldBe 5
    subtraction(3, 5) shouldBe 0
  }

  test("power is correct") {
    power(0, 1) shouldBe 0
    power(1, 0) shouldBe 1
    power(1, 1) shouldBe 1
    power(2, 2) shouldBe 4
    power(3, 3) shouldBe 27
    power(2, 10) shouldBe 1024
  }

  test("lessThanEqual is correct") {
    lessThanEqual(0, 1) shouldBe true
    lessThanEqual(0, 0) shouldBe true
    lessThanEqual(1, 1) shouldBe true
    lessThanEqual(3, 8) shouldBe true
    lessThanEqual(1, 0) shouldBe false
    lessThanEqual(4, 3) shouldBe false
  }

  test("min is correct") {
    min(0, 1) shouldBe 0
    min(0, 0) shouldBe 0
    min(1, 1) shouldBe 1
    min(3, 8) shouldBe 3
    min(1, 0) shouldBe 0
    min(4, 3) shouldBe 3
  }
  
  test("quotient is correct") {
    quotient(6, 3) shouldBe 2
    quotient(4, 1) shouldBe 4
    quotient(5, 2) shouldBe 2
    quotient(0, 4) shouldBe 0
    an[IllegalArgumentException] should be thrownBy quotient(8, 0)
  }

  test("remainder is correct") {
    remainder(6, 3) shouldBe 0
    remainder(4, 1) shouldBe 0
    remainder(5, 2) shouldBe 1
    remainder(9, 5) shouldBe 4
    remainder(0, 4) shouldBe 0
    an[IllegalArgumentException] should be thrownBy remainder(8, 0)
  }
  
  test("gcd is correct") {
    gcd(1, 3) shouldBe 1
    gcd(3, 1) shouldBe 1
    gcd(6, 9) shouldBe 3
    gcd(9, 6) shouldBe 3
    gcd(5, 5) shouldBe 5 
  }
  
  test("tetration is correct") {
    tetration(0, 2) shouldBe 1
    tetration(1, 4) shouldBe 1
    tetration(2, 2) shouldBe 4
    tetration(2, 3) shouldBe 16
    tetration(0, 0) shouldBe 1
  }
}