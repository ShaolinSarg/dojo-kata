package newcastle.dojo.kata

import org.scalatest.Matchers
import org.scalatest.FlatSpec
import scala.language.higherKinds

class MonadInstSpec extends FlatSpec with Matchers {

  trait Monad[M[_]] {
    def unit[A](a: A): M[A]
    def map[A, B](m: M[A])(func: A => B): M[B] = flatMap(m)(a => unit(func(a)))
    def flatMap[A, B](f: M[A])(func: A => M[B]): M[B]
    def map2[A, B, C](ma: M[A], mb: M[B])(f: (A, B) => C): M[C] = flatMap(ma)(a => map(mb)(b => f(a,b)))
  }

  trait MonadPlus[M[_]] extends Monad[M] {
    def zero[A]: M[A]

    def plus[A](l: M[A], r: M[A]): M[A]

    def sum[A](l: List[M[A]]): M[A] =
      l.foldLeft(zero: M[A])(plus(_, _))

    def filter[A](m: M[A])(f: A => Boolean): M[A] =
      flatMap(m)(a => if (f(a)) unit(a) else zero)
  }

  def mapMonad[A, B, TC[_]](tc: TC[A])(f: A => B)(implicit monad: Monad[TC]) =
    monad.map(tc)(f)

  def sumMonad[A, TC[_]](ltc: List[TC[A]])(implicit monad: MonadPlus[TC]) =
    monad.sum(ltc)

  def filterMonad[A, TC[_]](tc: TC[A])(f: A => Boolean)(implicit monad: MonadPlus[TC]) =
    monad.filter(tc)(f)

  def map2Monad[A, B, C, TC[_]](tc: TC[A], tc1: TC[B])(f: (A, B) => C)(implicit monad: Monad[TC]) =
    monad.map2(tc, tc1)(f)

  implicit val listMonad = new MonadPlus[List] {
    def zero[A]: List[A] = Nil

    def plus[A](l: List[A], r: List[A]) = l ++ r

    def unit[A](a: A): List[A] =
      a :: Nil

    def flatMap[A, B](f: List[A])(func: A => List[B]): List[B] =
      f flatMap func
  }

  implicit val optionMonad = new MonadPlus[Option] {
    def zero[A]: Option[A] = None

    def plus[A](l: Option[A], r: Option[A]): Option[A] = l.orElse(r)

    def flatMap[A, B](f: Option[A])(func: (A) => Option[B]): Option[B] = f flatMap func

    def unit[A](a: A): Option[A] = Some(a)
  }

  "mapMonad" should "add one to each item in a list" in {
    mapMonad(List(1, 2, 3))(a => a + 1) shouldBe List(2, 3, 4)
  }

  "mapMonad" should "subtract two from each item in a list" in {
    mapMonad(List(1, 2, 3))(a => a - 2) shouldBe List(-1, 0, 1)
  }

  "mapMonad" should "return an empty list when mapping an empty list" in {
    mapMonad(List[Int]())(a => a - 2) shouldBe List()
  }

  it should "add one to an option if it is Some" in {
    mapMonad(Some(1): Option[Int])(a => a + 1) shouldBe Some(2)
    mapMonad(None: Option[Int])(a => a + 1) shouldBe None
  }

  "sumMonad" should "append lists" in {
    sumMonad(List(List(1, 2, 3), List(1, 2), List(4, 5, 6))) shouldBe List(1, 2, 3, 1, 2, 4, 5, 6)
  }

  it should "pick the first defined option" in {
    sumMonad(List(None, None, None, Some(1), None, Some(3))) shouldBe Some(1)
  }

  "filterMonad" should "filter a list" in {
    filterMonad(List(1, 2, 3, 4))(a => a % 2 == 0) shouldBe List(2, 4)
    filterMonad(List(1, 3, 5))(a => a % 2 == 0) shouldBe List()
    filterMonad(List[Int]())(a => a % 2 == 0) shouldBe List()
  }

  it should "filter an option" in {
    filterMonad(Some(2): Option[Int])(a => a % 2 == 0) shouldBe Some(2)
    filterMonad(Some(1): Option[Int])(a => a % 2 == 0) shouldBe None
    filterMonad(None: Option[Int])(a => a % 2 == 0) shouldBe None
  }

  "map2Monad" should "combine Options" in {
    map2Monad(Some(1): Option[Int], Some(4))((a, b) => a + b) shouldBe Some(5)
    map2Monad(None: Option[Int], Some(4))((a, b) => a + b) shouldBe None
  }

  //"traverseMonad" should "sequence a list, applying a mapping function" in {
    //val loa: List[Option[Int]] = List(Some(1), Some(2), Some(3))
    //val loi: List[Option[Int]] = List(Some(1), None, Some(3))
    //traverseMonad(loa)(i => Some(i.toString)) shouldBe Some(List("1", "2", "3"))
    //traverseMonad(loi)(i => Some(i.toString)) shouldBe None
  //}
}