package newcastle.dojo.kata

import org.scalatest.{ FlatSpec, Matchers }

class MonoidSpec() extends FlatSpec with Matchers {

  def setIsClosedUnderOperation[T](elements: Set[T])(operation: (T, T) => T) =
    {
      (for (x <- elements; y <- elements) yield (x, y)).filter(x => !elements.contains(operation(x._1, x._2))).isEmpty
    }

  def setHasIdentityUnderOperation[T](elements: Set[T])(operation: (T, T) => T) =
    {
      elements.filter(x => isIdentity(x, elements)(operation)).size == 1
    }

  def isIdentity[T](i: T, e: Set[T])(operation: (T, T) => T): Boolean =
    {
      e.filter(x => operation(i, x) != x || operation(x, i) != x).isEmpty
    }

// For a complete definition of a group, we also need these:

  def functionIsAssociativeInSet[T](elements: Set[T])(op: (T, T) => T): Boolean = {
      !(for (a<-elements;b<-elements;c<-elements) yield(a,b,c)).exists{case (a,b,c) => op(op(a,b),c) != op(a,op(b,c))}
  }
  
  def eachElementInSetHasAnInverseUnderOperation[T](elements: Set[T])(op: (T, T) => T): Boolean = {
  val identityInSet = elements.find(isIdentity(_, elements)(op)).getOrElse(throw new AssertionError("no identity element in set"))
  elements.forall(x =>
      elements.exists(y => op(x,y) == identityInSet || op(y,x) == identityInSet))
  }
  
  val integers0to5 = Set(0, 1, 2, 3, 4, 5)
  def additionModulo6(a: Int, b: Int) = (a + b) % 6
  def additionThenDoubleModulo6(a: Int, b: Int) = ((a + b) * 2) % 6

  "The group of integers 0-5 under addition modulo 6" should "be closed" in {
    setIsClosedUnderOperation(integers0to5)(additionModulo6) shouldBe true
  }
  it should "have an identity" in {
    setHasIdentityUnderOperation(integers0to5)(additionModulo6) shouldBe true
  }

  "The group of integers 0-5 under multiplication" should "not be closed" in {
    setIsClosedUnderOperation(integers0to5)(_ * _) shouldBe false
  }
  it should "have an identity" in {
    setHasIdentityUnderOperation(integers0to5)(_ * _) shouldBe true
  }

  "The group of integers 0-5 under addition, with one extra, modulo 6" should "be closed" in {
    setIsClosedUnderOperation(integers0to5)(additionThenDoubleModulo6) shouldBe true
  }
  it should "not have an identity" in {
    setHasIdentityUnderOperation(integers0to5)(additionThenDoubleModulo6) shouldBe false
  }
  
   it should "identify nonassociative functions in double sets" in {
    assert(!functionIsAssociativeInSet((1 to 5).map(_.asInstanceOf[Double]).toSet[Double])  (_/_) )
  }

  it should "identify associative functions in int sets" in {
    val associativeFunctions:Set[(Int,Int)=>Int] =
      Set( _ + _, _ * _ )
    for (op <- associativeFunctions) {
      assert(functionIsAssociativeInSet((1 to 10).toSet[Int])(op))
    }
  }

}
