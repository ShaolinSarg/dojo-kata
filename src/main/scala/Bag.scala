package newcastle.dojo.kata

import scala.collection.immutable.HashMap

class Bag[A] (underlying : HashMap[A, Int]) {
  
  def add(a: A): Bag[A] = new Bag(underlying.updated(a, numberOf(a) + 1))

  def delete(a: A): Bag[A] = new Bag(underlying.updated(a, numberOf(a) - 1))

  def numberOf(a: A): Int = underlying.getOrElse(a, 0)

  override def toString(): String = {
    "[Bag[" + underlying.mkString(",") + "]]"
  }
}