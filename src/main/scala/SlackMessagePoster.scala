package bomb

import scalaj.http._
import play.api.libs.json._
import com.typesafe.config._

object SlackMessagePoster {
  case class Message(channel: String, username: String, text: String, icon_emoji: String)
  case class SlackMessageException(msg: String) extends Exception(msg)
  implicit val msgFormat = Json.format[Message]

  lazy val conf = ConfigFactory.load("slack").getConfig("slack-conf")
  lazy val url = conf.getString("url")
  lazy val defaultMessage = Message(conf.getString("channel"), conf.getString("username"), "dummyMessage", conf.getString("icon"))

  def sendMessageToSlack(myText: String) = {
    val message = defaultMessage.copy(text = myText)
    val payload = Json.toJson(message)
    val response = Http(url).postData(s"payload=$payload").asString
    if (!response.is2xx)
      throw new SlackMessageException(s"Failed to post message : ${response.toString}")
  }

}