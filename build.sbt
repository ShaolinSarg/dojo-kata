val scalaTest = "org.scalatest" %% "scalatest" % "2.2.4" % "test"
val scalaCheck = "org.scalacheck" %% "scalacheck" % "1.12.4" % "test"
val scalajHttp = "org.scalaj" %% "scalaj-http" % "1.1.4"
val playJson =  "com.typesafe.play" %% "play-json" % "2.3.9"
val config = "com.typesafe" % "config" % "1.2.1"
val liftweb = "net.liftweb" % "lift-json_2.11" % "3.0-M5-1"
val httpClient = "org.apache.httpcomponents" % "httpclient" % "4.5"



lazy val commonSettings = Seq(
  organization := "newcastle.dojo",
  version := "0.1.0",
  scalaVersion := "2.11.7"
)

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings(scalariformSettings: _*).
  settings(
    name := "Dojo Kata",
    libraryDependencies ++= Seq(scalaTest,scalaCheck,scalajHttp,playJson,config,liftweb,httpClient)
)